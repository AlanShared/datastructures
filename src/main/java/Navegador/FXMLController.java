package Navegador;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FXMLController implements Initializable {
    
    @FXML 
    private Label label;
    
    @FXML
    WebView mywebview;
    

    
    @FXML
    private void handleButtonAction(ActionEvent event) {                
        mywebview.getEngine().load("http://google.com");
        label.setVisible(true);        
    }
    
     @FXML
    public void accionregresar(ActionEvent evento) {                           
         ((Node)(evento.getSource())).getScene().getWindow().hide();//cerrar ventana
         main();
    }
    private void main() {
        Stage stage = new Stage();        
        
        
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");
        
        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
}
