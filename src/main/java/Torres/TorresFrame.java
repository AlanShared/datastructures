/*
 frame.setLayout(new floLayout(FlowLayout.CENTER));
frame.setLayout(new floLayout(FlowLayout.RIGHT));
frame.setLayout(new floLayout(FlowLayout.LEFT));
El FlowLayout, es aquel layout q ubica a todos los componentes en forma horizontal, en el orden q le digamos.
 */
package Torres;

/**
 *
 * @author CinthyaLiliana
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;

public class TorresFrame extends JFrame {

    private Hanoi h;
    private boolean resolver;

    public TorresFrame(int ndiscos) {
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        setLayout(new FlowLayout());
        h = new Hanoi(ndiscos);
        resolver = true;
        this.setBackground(Color.white);
        
        
        
    }
    
    

    public void paint(Graphics g) {

        h.dibuja(g);
        if (resolver) {
            h.resuelve(g);
        }
    }

}
