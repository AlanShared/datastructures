package ListasDobles;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class FXMLController implements Initializable {
    
    DoublyLinkedList link = new DoublyLinkedList();
    String texto;
    long array[] = new long[30];
    int c = 0;
    @FXML
    private Label label25, label1, label2, label3, label4, label5, label6, label7, label8,
            label9, label10, label11, label12, label13, label14, label15, label16,
            label17, label18, label19, label20, label21, label22, label23, label24, here;
    
    @FXML
    private TextField textfield;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {

//        String button = event.getSource().toString();  //saber que boton genera el evento
        if (textfield.getText().isEmpty()) {
            
            JOptionPane.showMessageDialog(null, "La caja de texto no puede estar vacia, ingresa un numero");
        } else {
            if (c < 26) {
                int data = Integer.parseInt(textfield.getText());
                
                link.insertFirst(data);
                c++;
                inde();
            }
        }
    }
    
    @FXML
    private void accionBuscar(ActionEvent e) {
        if (textfield.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "La caja esta vacia, porfavor ingresa un dato");
            
        } else {
            if (!link.isEmpty()) {
                int key = Integer.parseInt(textfield.getText());
                
                link.find(key);
                if (link.find(key) != null) {
                    int pos = link.position(key);
                    found(pos);
                } else {
                    JOptionPane.showMessageDialog(null, "No se encontró el numero buscado");
                }
            } else {
                JOptionPane.showMessageDialog(null, "La lista esta vacia, no hay elementos que buscar");
            }
        }
        
    }
    
    @FXML
    private void accionEliminar(ActionEvent e) {
        
        if (textfield.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "La caja de texto no puede estar vacia, ingresa un numero");
        } else {
            
            if (!link.isEmpty()) {
                int key = Integer.parseInt(textfield.getText());
                //link.deleteKey(key);
                
                if (link.deleteKey(key) != null) {
                    c--;
                    inde();
                } else {
                    JOptionPane.showMessageDialog(null, "No existe el numero");
                }
                
            } else {
                JOptionPane.showMessageDialog(null, "La lista esta vacia, no puedes eliminar");
            }
        }
    }
    
    @FXML
    private void accionOrdenar(ActionEvent ev) {
        link.bubbleSort();
        inde();
        
    }
    
    public void inde() {
        
        array = link.arreglo();
//      aqui.setLocation(label1.getX(), label1.getY() + 25);
        here.setLayoutX(label1.getLayoutX());
        here.setLayoutY(label1.getLayoutY() + 85);
        
        switch (link.num()) {
            case 1:
                label1.setText("" + array[0]);
                label2.setText("");
                break;
            
            case 2:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("");
                break;
            
            case 3:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("");
                break;
            
            case 4:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("");
                break;
            
            case 5:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("");
                break;
            
            case 6:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("");
                break;
            
            case 7:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("");
                break;
            
            case 8:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("");
                break;
            
            case 9:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("");
                break;
            
            case 10:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("");
                break;
            
            case 11:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("");
                break;
            
            case 12:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("");
                break;
            
            case 13:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("");
                break;
            
            case 14:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("");
                break;
            
            case 15:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("");
                break;
            
            case 16:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("");
                break;
            
            case 17:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("");
                break;
            
            case 18:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("");
                break;
            
            case 19:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("");
                break;
            
            case 20:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("" + array[19]);
                label21.setText("");
                break;
            
            case 21:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("" + array[19]);
                label21.setText("" + array[20]);
                label22.setText("");
                break;
            
            case 22:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("" + array[19]);
                label21.setText("" + array[20]);
                label22.setText("" + array[21]);
                label23.setText("");
                break;
            
            case 23:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("" + array[19]);
                label21.setText("" + array[20]);
                label22.setText("" + array[21]);
                label23.setText("" + array[22]);
                label24.setText("");
                break;
            
            case 24:
                label1.setText("" + array[0]);
                label2.setText("" + array[1]);
                label3.setText("" + array[2]);
                label4.setText("" + array[3]);
                label5.setText("" + array[4]);
                label6.setText("" + array[5]);
                label7.setText("" + array[6]);
                label8.setText("" + array[7]);
                label9.setText("" + array[8]);
                label10.setText("" + array[9]);
                label11.setText("" + array[10]);
                label12.setText("" + array[11]);
                label13.setText("" + array[11]);
                label14.setText("" + array[13]);
                label15.setText("" + array[14]);
                label16.setText("" + array[15]);
                label17.setText("" + array[16]);
                label18.setText("" + array[17]);
                label19.setText("" + array[18]);
                label20.setText("" + array[19]);
                label21.setText("" + array[20]);
                label22.setText("" + array[21]);
                label23.setText("" + array[22]);
                label24.setText("" + array[23]);
                label25.setText("");
                break;
            
        }
        
    }
    
    public void found(int pos) {
        
        switch (pos + 1) {
            
            case 1:
                here.setLayoutX(label1.getLayoutX());
                here.setLayoutY(label1.getLayoutY() + 85);
                break;
            case 2:
                here.setLayoutX(label2.getLayoutX());
                here.setLayoutY(label2.getLayoutY() + 85);
                break;
            case 3:
                here.setLayoutX(label3.getLayoutX());
                here.setLayoutY(label3.getLayoutY() + 85);
                break;
            case 4:
                here.setLayoutX(label4.getLayoutX());
                here.setLayoutY(label4.getLayoutY() + 85);
                break;
            case 5:
                here.setLayoutX(label5.getLayoutX());
                here.setLayoutY(label5.getLayoutY() + 85);
                break;
            case 6:
                here.setLayoutX(label6.getLayoutX());
                here.setLayoutY(label6.getLayoutY() + 85);
                break;
            case 7:
                here.setLayoutX(label7.getLayoutX());
                here.setLayoutY(label7.getLayoutY() + 85);
                break;
            case 8:
                here.setLayoutX(label8.getLayoutX());
                here.setLayoutY(label8.getLayoutY() + 85);
                break;
            case 9:
                here.setLayoutX(label9.getLayoutX());
                here.setLayoutY(label9.getLayoutY() + 85);
                break;
            case 10:
                here.setLayoutX(label10.getLayoutX());
                here.setLayoutY(label10.getLayoutY() + 85);
                break;
            case 11:
                here.setLayoutX(label11.getLayoutX());
                here.setLayoutY(label11.getLayoutY() + 85);
                break;
            case 12:
                here.setLayoutX(label12.getLayoutX());
                here.setLayoutY(label12.getLayoutY() + 85);
                break;
            case 13:
                here.setLayoutX(label13.getLayoutX());
                here.setLayoutY(label13.getLayoutY() + 85);
                break;
            case 14:
                here.setLayoutX(label14.getLayoutX());
                here.setLayoutY(label14.getLayoutY() + 85);
                break;
            case 15:
                here.setLayoutX(label15.getLayoutX());
                here.setLayoutY(label15.getLayoutY() + 85);
                break;
            case 16:
                here.setLayoutX(label16.getLayoutX());
                here.setLayoutY(label16.getLayoutY() + 85);
                break;
            case 17:
                here.setLayoutX(label17.getLayoutX());
                here.setLayoutY(label17.getLayoutY() + 85);
                break;
            case 18:
                here.setLayoutX(label18.getLayoutX());
                here.setLayoutY(label18.getLayoutY() + 85);
                break;
            case 19:
                here.setLayoutX(label19.getLayoutX());
                here.setLayoutY(label19.getLayoutY() + 85);
                break;
            case 20:
                here.setLayoutX(label20.getLayoutX());
                here.setLayoutY(label20.getLayoutY() + 85);
                break;
            case 21:
                here.setLayoutX(label21.getLayoutX());
                here.setLayoutY(label21.getLayoutY() + 85);
                break;
            case 22:
                here.setLayoutX(label22.getLayoutX());
                here.setLayoutY(label22.getLayoutY() + 85);
                break;
            case 23:
                here.setLayoutX(label23.getLayoutX());
                here.setLayoutY(label23.getLayoutY() + 85);
                break;
            case 24:
                here.setLayoutX(label24.getLayoutX());
                here.setLayoutY(label24.getLayoutY() + 85);
                break;
            case 25:
                here.setLayoutX(label25.getLayoutX());
                here.setLayoutY(label25.getLayoutY() + 85);
                break;
            
        }
    }
    
    
    @FXML
    public void accionregresar(ActionEvent evento) {                           
         ((Node)(evento.getSource())).getScene().getWindow().hide();//cerrar ventana
         main();
    }
    private void main() {
        Stage stage = new Stage();        
        
        
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");
        
        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
}
