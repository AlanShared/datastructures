package Expresiones;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    @FXML
    private TextField txtescribe;
    @FXML
    private RadioButton rbiap;
    @FXML
    private RadioButton rbodp;
    @FXML
    private Button btnconvertir;
    @FXML
    private Label lblmostrar;

    @FXML
    private Button btnregresar;
    
    
    @FXML
    private void accionconvertir(ActionEvent evento) {
        String input, output;
        input = txtescribe.getText();
        int output1;
        InToPost theTrans = new InToPost(input);
        //ParsePost aParser = new ParsePost(input);
        if (rbiap.isSelected()) {
            
            output = theTrans.doTrans();
            lblmostrar.setText("El Posfijo es " + output + '\n');            
        } else {
            if (rbodp.isSelected()) {
           output = theTrans.doTrans();
           ParsePost aParser = new ParsePost(output);
            output1 = aParser.doParse();
                lblmostrar.setText("El resultado es " + output1 + '\n');
            } else {
                lblmostrar.setText("Selecciona una opción.");
            }
        }
    }
    
     @FXML
    public void accionregresar(ActionEvent evento) {                           
         ((Node)(evento.getSource())).getScene().getWindow().hide();//cerrar ventana
         main();
    }
    private void main() {
        Stage stage = new Stage();        
        
        
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");
        
        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
