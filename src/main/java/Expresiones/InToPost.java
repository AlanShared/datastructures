/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Expresiones;

/**
 *
 * @author Andresiac
 */
public class InToPost {

    private StackInf theStack;
    private String input;
    private String output = "";
//-------------------------------------------------------------- 

    public InToPost(String in) // constructor
    {
        input = in;
        int stackSize = input.length();
        theStack = new StackInf(stackSize);
    }

    public String doTrans() // do translation to postfix 
    {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            theStack.displayStack("For " + ch + " ");
            switch (ch) {
                case '+':
                case '-':

                    gotOper(ch, 1);

                    break;
                case '*':
                case '/':
                case '%':

                    gotOper(ch, 2);
                    break;
                case '(':
                    theStack.push(ch);
                    break;

                case ')':
                    gotParen(ch);
                    break;
                default:
                    output= output+ch;
                    break;
            } // end switch
        } // end for
        while (!theStack.isEmpty()) // pop remaining opers
        {
            theStack.displayStack("While "); // *diagnostic* 
            output = output + theStack.pop(); // write to output 
        }
        theStack.displayStack("End "); // *diagnostic* 
        return output; // return postfix
    } // end doTrans()

    //-------------------------------------------------------------- 
    public void gotOper(char opThis, int prec1) { // got operator from input 
        while (!theStack.isEmpty()) {
            char opTop = theStack.pop();
            if (opTop == '(') {
                theStack.push(opTop);
                break;
            } else {
                int prec2;

                if (opTop == '+' || opTop == '-') // find new op prec 
                {
                    prec2 = 1;
                } else {
                    prec2 = 2;
                }
                if (prec2 < prec1) // if prec of new op less 
                {// than prec of old 
                    theStack.push(opTop); // save newly-popped op 
                    break;
                } else // prec of new not less
                {
                    output = output + opTop; // than prec of old 
                }
            } // end else (it’s an operator)
        } // end while
        theStack.push(opThis); // push new operator 
    } // end gotOp()

    //-------------------------------------------------------------- 
    public void gotParen(char ch) {
        while (!theStack.isEmpty()) {
            char chx = theStack.pop();
            if (chx == '(') {
                break;
            } else {
                output = output + chx;
            }

        }
    }
}
