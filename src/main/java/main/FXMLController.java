package main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    
    @FXML
    private void arreglos(ActionEvent event) {
        ventanaArrays();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void ordenar(ActionEvent event) {
        ventanaOrdenacion();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void tiempo(ActionEvent event) {
        ventanaTiempos();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void pilas(ActionEvent event) {
        ventanaPilas();//abrir ventana
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void colas(ActionEvent event) {
        ventanaColas();//abrir ventana
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void parsing(ActionEvent event) {
        ventanaparsing();//abrir ventana
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void linkList(ActionEvent event) {
        ventanaListaDoble();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void torresHanoi(ActionEvent event) {
        ventanaHanoi();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void arbol(ActionEvent event) {
        ventanaArbol();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void Navegador(ActionEvent event) {
        ventanaNav();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
        
    }
    
    @FXML
    private void Info(ActionEvent event) {
        ventanaInfo();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @FXML
    private void salir(ActionEvent event) {
        ventanaIntro();
        ((Node) (event.getSource())).getScene().getWindow().hide();//cerrar ventana
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private void ventanaIntro() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Intro.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Style_Intro.css");
        stage.setTitle("Estructura de Datos & Algoritmos");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaArrays() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Arrays.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesArrays.css");
        stage.setTitle("Arrays");
        stage.setScene(scene);
        stage.show();
    }
    
        private void ventanaOrdenacion() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Ordenacion.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        stage.setTitle("Ordenación");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaTiempos() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Tiempos.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesTiempos.css");
        stage.setTitle("Tiempos");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaPilas() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Pilas.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesPilas.css");
        stage.setTitle("Pilas");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaColas() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Colas.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesColas.css");
        stage.setTitle("Colas");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaparsing() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_expresion.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesConv.css");
        stage.setTitle("Conversiones");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaListaDoble() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_ListasDobles.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesLD.css");
        stage.setTitle("Listas Ligadas");
        stage.setScene(scene);
        stage.show();
    }

        private void ventanaHanoi() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Torres.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        stage.setTitle("Torres de Hanoi");
        stage.setScene(scene);
        stage.show();
    }
    
    
    private void ventanaArbol() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Arbol.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesArbol.css");
        stage.setTitle("Árbol");
        stage.setScene(scene);
        stage.show();
    }

    private void ventanaNav() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Nave.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesNave.css");
        stage.setTitle("Acerca de...");
        stage.setScene(scene);
        stage.show();
    }
    
    private void ventanaInfo() {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/S_Info.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesInfo.css");
        stage.setTitle("Acerca de...");
        stage.setScene(scene);
        stage.show();
    }

}
