package Arrays;

import javax.swing.JOptionPane;

public class OrdArray {

    private int nElems;
    private long a[];

    public OrdArray(int n) {
        this.nElems = 0;
        this.a = new long[n];
    }

    public int size() {
        return this.nElems;
    }

    public int findBin(long searchKey) { //binary search
        int lowerBound = 0;
        int upperBound = nElems - 1;
        int curIn = 0;
        while (true) {
            curIn = (lowerBound + upperBound) / 2;
            
            if (a[curIn] == searchKey) {
//                frmOrdered.moverFlechita(curIn, 20);
                return curIn;              // found it
            } else if (lowerBound > upperBound) {
//                frmOrdered.moverFlechita(curIn, 20);
                return nElems;             // can’t find it
            } else // divide range
            {
//                frmOrdered.moverFlechita(curIn, 20);
                if (a[curIn] < searchKey) {
                    lowerBound = curIn + 1; // it’s in upper half
                } else {
                    upperBound = curIn - 1; // it’s in lower half
                }
            }  // end else divide range        
            
        }  // end while
    }

    public int linearSearch(long searchKey) {
        int p = -1;
        for (int i = 0; i < nElems; i++) {
            
            if (a[i] == searchKey) {
               //pos(i);
                p = i;
                break;
            }
            
        }
        return p;
    }

    public int insert(long value) // put element into array
    {
        int j, pos;
        for (j = 0; j < nElems; j++) // find where it goes
        {
            if (a[j] > value) // (linear search)
            {
                break;
            }
        }
        for (int k = nElems; k > j; k--) // move bigger ones up
        {
            a[k] = a[k - 1];
        }
        a[j] = value;                  // insert it
        pos = j;
        nElems++;                      // increment size
        return pos;
    }

    public boolean delete(long value) {
        int j = findBin(value); ////////////////////////////////////////////////////////////////////////////////////
        if (j == nElems) // can’t find it
        {
            return false;
        } else // found it
        {
            for (int k = j; k < nElems; k++) // move bigger ones down
            {
                a[k] = a[k + 1];
            }
            nElems--;                   // decrement size
            return true;
        }
    }

    public void display() // displays array contents
    {
        for (int j = 0; j < nElems; j++) // for each element,
        {
            System.out.print(a[j] + " ");  // display it
            JOptionPane.showMessageDialog(null, a[j] + " ");// display with joptionpane
            System.out.println("");
        }
    }
    
    public long getNum(int pos){
       return a[pos];
    }
    
    public int pos(int posicion){
        return posicion;
    }
    
}
