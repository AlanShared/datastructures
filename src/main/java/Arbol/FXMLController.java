package Arbol;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    @FXML
    private Button btnFill;
    @FXML
    private Button btnFind;
    @FXML
    private Button btnIns;
    @FXML
    private Button btnDel;
    @FXML
    private TextField txtCaja;
    @FXML
    private Label lblText;
    @FXML
    private Label lblroot;
    @FXML
    private Label lbl1;
    @FXML
    private Label lbl2;
    @FXML
    private Label lbl3;
    @FXML
    private Label lbl4;
    @FXML
    private Label lbl5;
    @FXML
    private Label lbl6;
    @FXML
    private Label lbl7;
    @FXML
    private Label lbl8;
    @FXML
    private Label lbl9;
    @FXML
    private Label lbl10;
    @FXML
    private Label lbl11;
    @FXML
    private Label lbl12;
    @FXML
    private Label lbl13;
    @FXML
    private Label lbl14;

    Tree theTree;
    //int value;

    @FXML
    private void accionFill(ActionEvent event) {
        theTree = new Tree();

        btnFill.setDisable(true);
        btnFind.setDisable(false);
        btnIns.setDisable(false);
        btnDel.setDisable(false);
        txtCaja.setDisable(false);
//
//        theTree.insert(50, 1.5);
//        theTree.insert(25, 1.2);
//        theTree.insert(75, 1.7);
//        theTree.insert(12, 1.5);
//        theTree.insert(87, 1.2);
//        theTree.insert(37, 1.7);
//        theTree.insert(62, 1.5);

        if (!theTree.isEmpty()) {
            theTree.displayTree();
            preOrder(theTree.root, 0);
        }

    }

    int conta = 0;

    @FXML
    private void accionIns(ActionEvent event) {

        if (conta < 15) {
            if (!txtCaja.getText().isEmpty()) {
                int x = Integer.parseInt(txtCaja.getText());
                theTree.insert(x);
                lblText.setText("");
                conta++;
                if (!theTree.isEmpty()) {
                    preOrder(theTree.root, 0);
                }
            } else {
                lblText.setText("Ingresa un número!");
            }
        } else {
            lblText.setText("Árbol lleno!");
        }
    }

    @FXML
    private void accionDel(ActionEvent event) {
        if (!txtCaja.getText().isEmpty()) {

            int x = Integer.parseInt(txtCaja.getText());
            if (!theTree.isEmpty()) {

                boolean borro = theTree.delete(x);
                if (borro) {
                    if (!theTree.isEmpty()) {
                        displayeli();
                        preOrder(theTree.root, 0);
                    } else {
                        lblroot.setText("");
                    }
                    lblText.setText("Se eliminó " + x);
                } else {
                    lblText.setText("No se eliminó " + x);
                }
            } else {
                lblText.setText("El árbol esta vacío");
            }
        } else {
            lblText.setText("Ingresa un número!");
        }
    }

    @FXML
    private void accionFind(ActionEvent event) {
        if (!txtCaja.getText().isEmpty()) {
            int x = Integer.parseInt(txtCaja.getText());
            Node n = theTree.find(x);
            if (n != null) {
                lblText.setText("Se encontró el número " + x);
            } else {
                lblText.setText("No se encontró el número");
            }
        } else {
            lblText.setText("Ingresa un número!");
        }
    }

    String border = "-fx-border-color:black; -fx-border-width: 3; -fx-border-radius: 50;";

    private void display(int id, Node nod) {
        switch (id) {
            case 0:
                lblroot.setText("" + nod.iData);
                lblroot.setStyle(border);
                break;
            case 1:
                lbl1.setText("" + nod.iData);
                lbl1.setStyle(border);
                break;
            case 2:
                lbl2.setText("" + nod.iData);
                lbl2.setStyle(border);
                break;
            case 3:
                lbl3.setText("" + nod.iData);
                lbl3.setStyle(border);
                break;
            case 4:
                lbl4.setText("" + nod.iData);
                lbl4.setStyle(border);
                break;
            case 5:
                lbl5.setText("" + nod.iData);
                lbl5.setStyle(border);
                break;
            case 6:
                lbl6.setText("" + nod.iData);
                lbl6.setStyle(border);
                break;
            case 7:
                lbl7.setText("" + nod.iData);
                lbl7.setStyle(border);
                break;
            case 8:
                lbl8.setText("" + nod.iData);
                lbl8.setStyle(border);
                break;
            case 9:
                lbl9.setText("" + nod.iData);
                lbl9.setStyle(border);
                break;
            case 10:
                lbl10.setText("" + nod.iData);
                lbl10.setStyle(border);
                break;
            case 11:
                lbl11.setText("" + nod.iData);
                lbl11.setStyle(border);
                break;
            case 12:
                lbl12.setText("" + nod.iData);
                lbl12.setStyle(border);
                break;
            case 13:
                lbl13.setText("" + nod.iData);
                lbl13.setStyle(border);
                break;
            case 14:
                lbl14.setText("" + nod.iData);
                lbl14.setStyle(border);
                break;

            default:

        }

    }

    private void displayeli() {
        lblroot.setText("");
        lbl1.setText("");
        lbl2.setText("");
        lbl3.setText("");
        lbl4.setText("");
        lbl5.setText("");
        lbl6.setText("");
        lbl7.setText("");
        lbl8.setText("");
        lbl9.setText("");
        lbl10.setText("");
        lbl11.setText("");
        lbl12.setText("");
        lbl13.setText("");
        lbl14.setText("");

    }

    private void preOrder(Node localRoot, int c) {
        if (localRoot != null) {
            display(c, localRoot);
//            c+=c+1;
            preOrder(localRoot.leftChild, c + c + 1);
//            c+=c+2;
            preOrder(localRoot.rightChild, c + c + 2);

        }
    }

    @FXML
    public void accionregresar(ActionEvent evento) {
        ((javafx.scene.Node) (evento.getSource())).getScene().getWindow().hide();//cerrar ventana
        main();
    }

    private void main() {
        Stage stage = new Stage();

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");

        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
