package Pilas;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    @FXML
    private Label lbl1;
    @FXML
    private Label lbl2;
    @FXML
    private Label lbl3;
    @FXML
    private Label lbl4;
    @FXML
    private Label lbl5;
    @FXML
    private Label lbl6;
    @FXML
    private Label lbl7;
    @FXML
    private Label lbl8;
    @FXML
    private Label lbl9;
    @FXML
    private Label lbl10;
    @FXML
    private Label lblFront;
    @FXML
    private Label lblRear;
    @FXML
    private Label lblText;
    @FXML
    private Label colas;

    @FXML
    private Button btnNew;
    @FXML
    private Button btnIns;
    @FXML
    private Button btnDel;
    @FXML
    private Button btnPeek;

    @FXML
    private TextField txtCaja;

    Pila q;
    //JLabel front, rear;
    int tam;

    @FXML
    public void accionnew(ActionEvent evento) {

        tam = Integer.parseInt(txtCaja.getText());
        q = new Pila(tam);

        String border = "-fx-border-color:blue; -fx-background-color: gray;";

//        front.setLocation((int)lblFront.getLayoutX(), (int)lbl10.getLayoutY());
//        front.setVisible(true);
//        lblFront.setLayoutY(lbl10.getLayoutY());
//        
//        rear.setLocation((int)lblRear.getLayoutX(), (int)colas.getLayoutY());
//        rear.setVisible(true);
        lblRear.setLayoutY(colas.getLayoutY());
        if (tam > 10) {
            lblText.setText("Solo se pueden hasta de 10");
        } else {

            lblText.setText("");
        }

        switch (tam) {
            case 1:
                lbl10.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                lbl5.setStyle(null);
                lbl6.setStyle(null);
                lbl7.setStyle(null);
                lbl8.setStyle(null);
                lbl9.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 2:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
//                lbl10.setText("Hola");
//                lbl9.setText("Adios");

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                lbl5.setStyle(null);
                lbl6.setStyle(null);
                lbl7.setStyle(null);
                lbl8.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 3:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                lbl5.setStyle(null);
                lbl6.setStyle(null);
                lbl7.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 4:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                lbl5.setStyle(null);
                lbl6.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 5:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                lbl5.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 6:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);
                lbl5.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                lbl4.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 7:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);
                lbl5.setStyle(border);
                lbl4.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                lbl3.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 8:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);
                lbl5.setStyle(border);
                lbl4.setStyle(border);
                lbl3.setStyle(border);

                //
                lbl1.setStyle(null);
                lbl2.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 9:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);
                lbl5.setStyle(border);
                lbl4.setStyle(border);
                lbl3.setStyle(border);
                lbl2.setStyle(border);

                //
                lbl1.setStyle(null);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            case 10:
                lbl10.setStyle(border);
                lbl9.setStyle(border);
                lbl8.setStyle(border);
                lbl7.setStyle(border);
                lbl6.setStyle(border);
                lbl5.setStyle(border);
                lbl4.setStyle(border);
                lbl3.setStyle(border);
                lbl2.setStyle(border);
                lbl1.setStyle(border);
                btnIns.setDisable(false);
                btnDel.setDisable(false);
                btnPeek.setDisable(false);
                btnNew.setDisable(true);
//                lblFront.setVisible(true);
                lblRear.setVisible(true);
                break;
            default:
                lblText.setText("Solo se pueden hasta de 10");
                btnIns.setDisable(true);
                btnDel.setDisable(true);
                btnPeek.setDisable(true);
                btnNew.setDisable(false);
        }

    }

    @FXML
    public void accionIns(ActionEvent evento) {
        int x = Integer.parseInt(txtCaja.getText());

        if (!q.isFull()) {
            lblText.setText("");
            q.push(x);
            int y = q.posRear() + 1;
            switch (y) {
                case 1:
                    lbl10.setText(x + "");

                    //rear.setLocation(lblRear.getX(), lbl10.getY());
                    lblRear.setLayoutY(lbl10.getLayoutY());
                    //rear.setVisible(true);
                    break;
                case 2:
                    lbl9.setText(x + "");
                    lblRear.setLayoutY(lbl9.getLayoutY());

//                    rear.setLocation(lblRear.getX(), lbl9.getY());
//                    rear.setVisible(true);
                    break;
                case 3:
                    lbl8.setText(x + "");
                    lblRear.setLayoutY(lbl8.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl8.getY());
//                    rear.setVisible(true);
                    break;
                case 4:
                    lbl7.setText(x + "");
                    lblRear.setLayoutY(lbl7.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl7.getY());
//                    rear.setVisible(true);
                    break;
                case 5:
                    lbl6.setText(x + "");
                    lblRear.setLayoutY(lbl6.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl6.getY());
//                    rear.setVisible(true);
                    break;
                case 6:
                    lbl5.setText(x + "");
                    lblRear.setLayoutY(lbl5.getLayoutY());

//                    rear.setLocation(lblRear.getX(), lbl5.getY());
//                    rear.setVisible(true);
                    break;
                case 7:
                    lbl4.setText(x + "");
                    lblRear.setLayoutY(lbl4.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl4.getY());
//                    rear.setVisible(true);
                    break;
                case 8:
                    lbl3.setText(x + "");
                    lblRear.setLayoutY(lbl3.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl3.getY());
//                    rear.setVisible(true);
                    break;
                case 9:
                    lbl2.setText(x + "");
                    lblRear.setLayoutY(lbl2.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl2.getY());
//                    rear.setVisible(true);
                    break;
                case 10:
                    lbl1.setText(x + "");
                    lblRear.setLayoutY(lbl1.getLayoutY());
//                    rear.setLocation(lblRear.getX(), lbl1.getY());
//                    rear.setVisible(true);
                    break;
                default:

            }
        } else {
            lblText.setText("La pila esta llena");
        }

    }

    int x=0;

    @FXML
    public void accionDel(ActionEvent evento) {

        if (!q.isEmpty()) {
            lblText.setText("");
            q.pop();

            if (q.posRear() != 0) {
                x = q.posRear();
            }else{
                x=0;
            }
            switch (x+1) {
                case 1:
                    lbl10.setText("");
                    lblRear.setLayoutY(lbl10.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl9.getY());
//                    front.setVisible(true);
                    break;
                case 2:
                    lbl9.setText("");
                    lblRear.setLayoutY(lbl9.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl8.getY());
//                    front.setVisible(true);
                    break;
                case 3:
                    lbl8.setText("");
                    lblRear.setLayoutY(lbl8.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl7.getY());
//                    front.setVisible(true);
                    break;
                case 4:
                    lbl7.setText("");
                    lblRear.setLayoutY(lbl7.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl6.getY());
//                    front.setVisible(true);
                    break;
                case 5:
                    lbl6.setText("");
                    lblRear.setLayoutY(lbl6.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl5.getY());
//                    front.setVisible(true);
                    break;
                case 6:
                    lbl5.setText("");
                    lblRear.setLayoutY(lbl5.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl4.getY());
//                    front.setVisible(true);
                    break;
                case 7:
                    lbl4.setText("");
                    lblRear.setLayoutY(lbl4.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl3.getY());
//                    front.setVisible(true);
                    break;
                case 8:
                    lbl3.setText("");
                    lblRear.setLayoutY(lbl3.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl2.getY());
//                    front.setVisible(true);
                    break;
                case 9:
                    lbl2.setText("");
                    lblRear.setLayoutY(lbl2.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl1.getY());
//                    front.setVisible(true);
                    break;
                case 10:
                    lbl1.setText("");
                    lblRear.setLayoutY(lbl1.getLayoutY());
//                    front.setLocation(lblFront.getX(), lbl10.getY());
//                    front.setVisible(true);
                    break;
                default:

            }
        } else {
            lblText.setText("La cola esta vacia");
        }
    }

    @FXML
    public void accionpeek(ActionEvent evento) {
        lblText.setText(q.peek() + "");
    }

    @FXML
    public void accionregresar(ActionEvent evento) {
        ((Node) (evento.getSource())).getScene().getWindow().hide();//cerrar ventana
        main();

    }

    private void main() {
        Stage stage = new Stage();

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");

        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }
}
