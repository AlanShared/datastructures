package Tiempos;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    final static String Bubble = "Bubble";
    final static String Selection = "Selection";
    final static String Insertion = "Insertion";
    final static String Merge = "Merge";
    final static String Shell = "Shell";
    final static String Quick = "Quick";
    final static String Radix = "Radix";
    final static String Heap = "Heap";
    
    
    
    private TextArea txtmetodos;

    @FXML
    private void accionboton(ActionEvent event) {
        //start();
        graficar();
    }
    
    @FXML
    private void graficar() {        
        Stage stage = new Stage();
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final StackedBarChart<String, Number> sbc = new StackedBarChart<String, Number>(xAxis, yAxis);
        final XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();

        stage.setTitle("Tiempos");
        sbc.setTitle("¿Que metodo es mejor?");
        xAxis.setLabel("Metodo");
        xAxis.setCategories(FXCollections.<String>observableArrayList(
                Arrays.asList(Bubble, Selection, Insertion, Merge, Shell, Quick, Heap)));
        yAxis.setLabel("Segundos");
        series1.setName("Segundos");
        series1.getData().add(new XYChart.Data<String, Number>(Bubble, Tiempos.burbuja()));
        series1.getData().add(new XYChart.Data<String, Number>(Selection, Tiempos.seleccion()));
        series1.getData().add(new XYChart.Data<String, Number>(Insertion, Tiempos.insercion()));
        series1.getData().add(new XYChart.Data<String, Number>(Merge, Tiempos.merge()));
        series1.getData().add(new XYChart.Data<String, Number>(Shell, Tiempos.shell()));
        series1.getData().add(new XYChart.Data<String, Number>(Quick, Tiempos.quick()));
        series1.getData().add(new XYChart.Data<String, Number>(Radix, Tiempos.radix()));
        series1.getData().add(new XYChart.Data<String, Number>(Heap, Tiempos.heap()));

        Scene scene = new Scene(sbc, 800, 600);
        sbc.getData().addAll(series1);
        stage.setScene(scene);
        stage.show();

    }
    
    
     @FXML
    public void accionregresar(ActionEvent evento) {                           
         ((Node)(evento.getSource())).getScene().getWindow().hide();//cerrar ventana
         main();
    }
    private void main() {
        Stage stage = new Stage();        
        
        
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");
        
        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();
        
    }
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
