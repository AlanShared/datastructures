/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tiempos;


import java.util.Scanner;

/**
 *
 * @author Andresiac
 */
public class Tiempos {
    public static float t1, t2, t3, t4, t5, t6, t7, t8;
    
    public  void tiempo() {
     burbuja();
        seleccion();
        insercion();
        merge();
        shell();
        quick();
        radix();
        heap();

        System.out.println("\n\n\nTiempos:\nBurbuja: " + t1 + "\nSeleccion: " + t2 + "\nInsercion: " + t3 + "\nMerge: " 
                + t4 + "\nShell: " + t5 + "\n Quick: " + t6 + "\nRadix: " + t7 + "\n Heap: " + t8 + "\n");
    }
    

    public static float burbuja() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;            // array size
        ArrayBub arr;                 // reference to array
        arr = new ArrayBub(maxSize);  // create the array
        for (int j = 0; j < maxSize; j++) {
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }

        arr.display();                // display items
        System.out.println("\n");
        arr.bubbleSort();             // bubble sort them
        System.out.println("\n");
        arr.display();                // display them again
        long tf = System.currentTimeMillis();
        return t1 = (tf - ti) / 1000f;
        //System.out.println(t1);
    }

    public static float seleccion() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;            // array size
        ArraySel arr;                 // reference to array
        arr = new ArraySel(maxSize);  // create the array
        for (int j = 0; j < maxSize; j++) {
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }

        arr.display();                // display items
        System.out.println("\n");
        arr.selectionSort();          // selection-sort them
        System.out.println("\n");
        arr.display();                // display them again

        long tf = System.currentTimeMillis();
        return t2 = (tf - ti) / 1000f;
        //System.out.println(t2);
    }

    public static float insercion() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;            // array size
        ArrayIns arr;                 // reference to array
        arr = new ArrayIns(maxSize);  // create the array
        for (int j = 0; j < maxSize; j++) {
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }

        arr.display();                // display items
        System.out.println("\n");
        arr.insertionSort();          // insertion-sort them
        System.out.println("\n");
        arr.display();                // display them again

        long tf = System.currentTimeMillis();
        return t3 = (tf - ti) / 1000f;
        //System.out.println(t3);
    }

    public static float merge() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;             // array size
        DArray arr;                    // reference to array
        arr = new DArray(maxSize);     // create the array
        for (int j = 0; j < maxSize; j++) {
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }

        arr.display();                 // display items
        System.out.println("\n");
        arr.mergeSort();               // merge sort the array
        System.out.println("\n");
        arr.display();                 // display items again

        long tf = System.currentTimeMillis();
        return t4 = (tf - ti) / 1000f;
        //System.out.println(t4);
    }

    public static float shell() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;
        ArraySh arr;
        arr = new ArraySh(maxSize);
        for (int j = 0; j < maxSize; j++) {
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }
        arr.display();
        System.out.println("\n");
        arr.shellSort();
        System.out.println("\n");
        arr.display();

        long tf = System.currentTimeMillis();
        return t5 = (tf - ti) / 1000f;
        // System.out.println(t5);
    }

    public static float quick() {
        long ti = System.currentTimeMillis();
        int maxSize = 100;             // array size
        ArrayIns1 arr;
        arr = new ArrayIns1(maxSize);  // create array
        for (int j = 0; j < maxSize; j++) // fill array with
        {                          // random numbers
            long n = (int) (java.lang.Math.random() * 99);
            arr.insert(n);
        }
        arr.display();                // display items
        System.out.println("\n");
        arr.quickSort();              // quicksort them
        System.out.println("\n");
        arr.display();                // display them again

        long tf = System.currentTimeMillis();
        return t6 = (tf - ti) / 1000f;
        //System.out.println(t6);
    }

    public static float radix() {
        long ti = System.currentTimeMillis();
        RadixSort ra;
        ra = new RadixSort();
        Scanner scan = new Scanner(System.in);
        System.out.println("Radix Sort Test\n");
        int maxSize = 100, i;

        int arr[] = new int[100];

        for (int j = 0; j < maxSize; j++) // fill array with
        {                          // random numbers
            arr[j] = (int) (java.lang.Math.random() * 99);

        }

        System.out.println("\n");
        for (i = 0; i < maxSize; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        ra.sort(arr);

        System.out.println("\n ");
        for (i = 0; i < maxSize; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        long tf = System.currentTimeMillis();
        return t7 = (tf - ti) / 1000f;
        //System.out.println(t7);
    }

    public static float heap() {
        long ti = System.currentTimeMillis();
        HeapSort h = new HeapSort();
        int maxSize = 100;

        int arr[] = new int[100];

        for (int j = 0; j < maxSize; j++) // fill array with
        {                          // random numbers
            arr[j] = (int) (java.lang.Math.random() * 99);

        }

        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        h.sort(arr);
        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        long tf = System.currentTimeMillis();
        return t8 = (tf - ti) / 1000f;
        //System.out.println(t8);
    }
}
